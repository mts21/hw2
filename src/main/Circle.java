package main;

import java.util.Objects;

public class Circle extends GeomFigure{
    private Integer center_x;
    private Integer center_y;
    private Integer radius;
    private String color;

    public Circle(Integer center_x, Integer center_y, Integer radius, String color) {
        this.center_x = center_x;
        this.center_y = center_y;
        this.radius = radius;
        this.color = color;
    }

    @Override
    public Double GetPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public Double GetArea() {
        return Math.PI * radius * radius;
    }

    public Integer getCenter_x() {
        return center_x;
    }

    public void setCenter_x(Integer center_x) {
        this.center_x = center_x;
    }

    public Integer getCenter_y() {
        return center_y;
    }

    public void setCenter_y(Integer center_y) {
        this.center_y = center_y;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public String getColor() {
        return color;
    }

    public void colorCircle(String new_color) {
        this.color = new_color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Circle circle = (Circle) o;
        return Objects.equals(center_x, circle.center_x) && Objects.equals(center_y, circle.center_y) && Objects.equals(radius, circle.radius) && Objects.equals(color, circle.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(center_x, center_y, radius, color);
    }

    @Override
    public String toString() {
        return "Circle{" +
                "center_x=" + center_x +
                ", center_y=" + center_y +
                ", radius=" + radius +
                ", color='" + color + '\'' +
                '}';
    }
}
