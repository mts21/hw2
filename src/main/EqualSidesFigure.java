package main;

public interface EqualSidesFigure {
    boolean checkEqualityOfSides();
}
