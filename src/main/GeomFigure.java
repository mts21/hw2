package main;

public abstract class GeomFigure {
    public abstract Double GetPerimeter();
    public abstract Double GetArea();
}
