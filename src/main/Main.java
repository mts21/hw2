package main;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(1, 2, 5, "red");
        circle.colorCircle("green");
        System.out.println("==Circle testing==");
        System.out.println(circle);
        System.out.printf("Area = %f, Perimeter = %f\n", circle.GetArea(), circle.GetPerimeter());

        Triangle triangle = new Triangle(5d, 5d, 5d);
        System.out.println("==Triangle testing==");
        System.out.println(triangle);
        System.out.printf("Area = %f, Perimeter = %f\n", triangle.GetArea(), triangle.GetPerimeter());

        Polygon polygon = new Polygon(new Point[]{
                new Point(1, 2),
                new Point(3 ,2),
                new Point(1, 5),
                new Point(2, 5),
                new Point(4, 5)
        });
        System.out.println("==Polygon testing==");
        polygon.printAllPoints();
        System.out.printf("\nArea = %f, Perimeter = %f\n", polygon.GetArea(), polygon.GetPerimeter());

        System.out.println("==Rhomb testing==");
        Rhomb rhomb = new Rhomb(new Point[]{
                new Point(-4, 0),
                new Point(0, 4),
                new Point(4, 0),
                new Point(0, -4)
        });
        rhomb.printAllPoints();
        System.out.printf("\nArea = %f, Perimeter = %f\n", rhomb.GetArea(), rhomb.GetPerimeter());
        System.out.printf("Does figure have equal sides? : %b", rhomb.checkEqualityOfSides());

    }
}
