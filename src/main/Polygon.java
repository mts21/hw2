package main;

import java.awt.*;
import java.util.Arrays;
import java.util.Objects;

public class Polygon extends GeomFigure implements WithAngles{
    private Integer pointCount;
    private Point[] points;

    public Polygon(Point[] points) {
        this.points = points;
        this.pointCount = points.length;
    }

    protected Double[] getSides(){
        Double[] sides = new Double[pointCount];
        for (int i = 1; i < pointCount; i++) {
            sides[i - 1] = points[i - 1].distance(points[i]);
        }
        sides[sides.length - 1] = points[0].distance(points[pointCount - 1]);
        return sides;
    }

    @Override
    public Double GetPerimeter() {
        Double perimeter = 0d;
        for (Double side: getSides()){
            perimeter += side;
        }
        return perimeter;
    }

    @Override
    public Double GetArea() {
        Double x = 0d;
        Double y = 0d;
        for (int i = 0; i < pointCount - 1; i++) {
            x += points[i].x * points[i + 1].y;
            y += points[i].y * points[i + 1].x;
        }
        x += points[pointCount - 1].x * points[0].y;
        y += points[pointCount - 1].y * points[0].x;

        return Math.abs((x - y) / 2);
    }

    @Override
    public void printAllPoints() {
        System.out.println("==ALL POINTS==");
        for (int i = 0; i < pointCount; i++) {
            System.out.printf("(x%d = %d, y%d = %d)\n", i, points[i].x, i, points[i].y);
        }
    }

    public Integer getPointCount() {
        return pointCount;
    }

    public Point[] getPoints() {
        return points;
    }

    public void setPoints(Point[] points) {
        this.points = points;
        this.pointCount = points.length;
    }

    @Override
    public String toString() {
        return "Polygon{" +
                "pointCount=" + pointCount +
                ", points=" + Arrays.toString(points) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Polygon polygon = (Polygon) o;
        return Objects.equals(pointCount, polygon.pointCount) && Arrays.equals(points, polygon.points);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(pointCount);
        result = 31 * result + Arrays.hashCode(points);
        return result;
    }
}
