package main;

import java.awt.*;
import java.math.BigDecimal;

public class Rhomb extends Polygon implements EqualSidesFigure{
    public Rhomb(Point[] points) {
        super(points);
        if (!checkEqualityOfSides()) throw new IllegalArgumentException("Sides are not equal! :(");
    }

    @Override
    public boolean checkEqualityOfSides() {
        Double[] sidesD = super.getSides();
        BigDecimal[] sides = new BigDecimal[sidesD.length];

        for (int i = 0; i < sidesD.length; i++) {
            sides[i] = new BigDecimal(sidesD[i]);
        }

        BigDecimal x = sides[0];

        for (BigDecimal side : sides){
            if (!side.equals(x)) return false;
        }
        return true;
    }
}
